AntiFeatures:Tracking,NonFreeAssets
Categories:Reading,Internet
License:Apache2
Web Site:https://github.com/HugoGresse/Anecdote/blob/HEAD/README.md
Source Code:https://github.com/HugoGresse/Anecdote
Issue Tracker:https://github.com/HugoGresse/Anecdote/issues
FlattrID:a819229e365698639727db15164b8bbf

Auto Name:Quote
Summary:Read your latest quote and feed in a lite app
Description:
Anecdote is a simple yet powerful application to read your favorites quotes
websites. It's very light, not intrusive, fast and simple.

Features:

* 9GAG feed (image and video)
* quotes feeds from fmylife.com (FML), Bash.org, viedemerde.fr (VDM), danstonchat.com (DTC) and more
* infinite scroll and pull to refresh
* no strange permissions, no background tasks, no ad
* add your own website with some HTML knowledge

It's open-source on [https://github.com/HugoGresse/Anecdote GitHub.]

The content displayed in the application do not belongs to Anecdote. Crash and
some usage informations (such as the share of an item) are sended to my own
Countly server
.

Repo Type:git
Repo:https://github.com/HugoGresse/Anecdote.git

Build:1.1.1,19
    commit=v1.1.1
    subdir=app
    gradle=oss
    srclibs=Countly@16.06.04
    prebuild=cp -r $Countly$/sdk ../ && \
        echo -e "\ninclude ':sdk'" >> ../settings.gradle && \
        sed -i -e '/fileTree/acompile project(":sdk") build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.1.1
Current Version Code:19
